addSbtPlugin("com.jsuereth" % "sbt-pgp" % "1.0.0")

addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "1.0")

addSbtPlugin("org.tpolecat" % "tut-plugin" % "0.4.2")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.2")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.3")
