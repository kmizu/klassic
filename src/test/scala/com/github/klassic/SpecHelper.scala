package com.github.klassic

import org.scalatest.{DiagrammedAssertions, FunSpec}

trait SpecHelper extends FunSpec with DiagrammedAssertions
