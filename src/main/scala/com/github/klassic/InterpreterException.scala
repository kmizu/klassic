package com.github.klassic

case class InterpreterException(message: String) extends RuntimeException(message)
